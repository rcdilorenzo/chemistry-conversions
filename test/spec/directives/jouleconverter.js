'use strict';

describe('Directive: jouleConverter', function () {

  // load the directive's module
  beforeEach(module('chemistryConversionsApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<joule-converter></joule-converter>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the jouleConverter directive');
  }));
});
