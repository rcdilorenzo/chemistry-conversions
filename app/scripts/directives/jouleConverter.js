'use strict';

angular.module('chemistryConversionsApp')
  .directive('jouleConverter', function () {
    return {
      templateUrl: "views/joule-converter.html",
      restrict: 'E',
      scope: {},
      controller: function($scope) {
        $scope.units = [{name: "nm"}, {name: "m"}];
        $scope.selectedUnit = $scope.units[0];

        $scope.output = 0.0;

        var lightSpeed = 3.00 * Math.pow(10, 8);
        var planckConstant = 6.63 * Math.pow(10, -34)
 
        $scope.convertToJoules = function() {
          if (!$scope.input || isNaN($scope.input)) {
            $scope.output = 0.0;
          } else {
            var lambdaInMeters = $scope.selectedUnit.name == "nm" ? $scope.input * Math.pow(10, -9) : $scope.input;
            $scope.output = ((planckConstant * lightSpeed)/lambdaInMeters).toPrecision(3);
          }
        }
      }
    };
  });
