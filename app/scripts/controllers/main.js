'use strict';

angular.module('chemistryConversionsApp')
  .controller('MainCtrl', function ($scope, $sce) {
    var lightSpeed = 3.00 * Math.pow(10, 8);

    $scope.frequencyOutput = 0.0;
    $scope.wavelengthOutput = 0.0;

    $scope.frequencyUnits = [{name: "nm"}, {name: "m"}];
    $scope.frequencyUnit = $scope.frequencyUnits[0];

    $scope.wavelengthUnits = [{name: "kHz"}, {name: "Hz"}];
    $scope.wavelengthUnit = $scope.wavelengthUnits[0];

    function toScientificNotation(numberString) {
      var result = numberString.replace("e+", " x 10<sup>")+"</sup>";
      return result;
    }

    $scope.convertToFrequency = function() {
      if (!$scope.nmInput || isNaN($scope.nmInput)) {
        $scope.frequencyOutput = 0.0;
      } else {
        var lambdaInMeters = $scope.frequencyUnit.name == "nm" ? $scope.nmInput * Math.pow(10, -9) : $scope.nmInput;
        // .replace("e+", " x 10<sup>")+"</sup>"
        $scope.frequencyOutput = (lightSpeed / lambdaInMeters).toPrecision(3);
      }
    }

    $scope.convertToWavelength = function() {
      if (!$scope.frequencyInput || isNaN($scope.frequencyInput)) {
        $scope.wavelengthOutput = 0.0;
      } else {
        var Hz = $scope.wavelengthUnit.name == "kHz" ? $scope.frequencyInput * 1000 : $scope.frequencyInput;
        $scope.wavelengthOutput = (lightSpeed / Hz).toPrecision(5);
      }
    }
  });
